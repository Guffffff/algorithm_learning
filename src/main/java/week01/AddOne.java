package week01;

/**
 * 加一
 */
public class AddOne {
    /**
     * 实现一、加法进位逻辑
     * @param digits
     * @return
     */
    public int[] plusOne(int[] digits) {
        boolean up = true;
        for (int i = digits.length - 1; i >= 0; i--){
            if(!up){
                continue;
            }
            if (digits[i] == 9){
                up = true;
                digits[i] = 0;
            }else{
                up = false;
                digits[i] = digits[i] + 1;
            }
        }
        if(up){
            int[] result = new int[digits.length + 1];
            result[0] = 1;
            for (int i = 1; i < result.length; i++){
                result[i] = digits[i-1];
            }
            digits = result;
        }
        return digits;
    }

    /**
     * 实现二、力扣参考答案
     * 思路：倒排找9
     */
    public int[] plusOneAnswer(int[] digits) {

        for(int i = digits.length - 1; i >= 0; i--){
            if (digits[i] != 9){
                digits[i] = digits[i] + 1;
                for (int j = i + 1; j <= digits.length - 1; j++){
                    digits[j] = 0;
                }
                return digits;
            }
        }
        // 数据为 999...
        int[] result = new int[digits.length + 1];
        result[0] = 1;
        return result;
    }

}
