package week01;

public class MyCircularDeque {

    int[] queue;
    int head = 0;
    int tail = 1;
    int size = 0;

    public MyCircularDeque(int k) {
        queue = new int[k];
    }

    public boolean insertFront(int value) {
        if(isFull()){
            return false;
        }
        int subscript = head;
        queue[subscript] = value;
        if (subscript == 0){
            subscript = queue.length-1;
        }else{
            subscript = head - 1;
        }
        head = subscript;
        size++;
        return true;
    }

    public boolean insertLast(int value) {
        if(isFull()){
            return false;
        }
        int subscript = tail;
        queue[subscript] = value;
        if (subscript == queue.length - 1){
            subscript = 0;
        }else{
            subscript = tail + 1;
        }
        tail = subscript;
        size++;
        return true;
    }

    public boolean deleteFront() {
        if(isEmpty()){
            return false;
        }
        int subscript;
        if (head == queue.length - 1){
            subscript = 0;
        }else{
            subscript = head + 1;
        }
        queue[subscript] = 0;
        head = subscript;
        size--;
        return true;
    }

    public boolean deleteLast() {
        if(isEmpty()){
            return false;
        }
        int subscript;
        if (tail == 0){
            subscript = queue.length-1;
        }else{
            subscript = tail - 1;
        }
        queue[subscript] = 0;
        tail = subscript;
        size--;
        return true;
    }

    public int getFront() {
        if(isEmpty()){
            return -1;
        }
        int subscript;
        if (head == queue.length - 1){
            subscript = 0;
        }else{
            subscript = head + 1;
        }
        return queue[subscript];
    }

    public int getRear() {
        if(isEmpty()){
            return -1;
        }
        int subscript;
        if (tail == 0){
            subscript = queue.length-1;
        }else{
            subscript = tail - 1;
        }
        return queue[subscript];
    }

    public boolean isEmpty() {
        return size == 0? true : false;
    }

    public boolean isFull() {
        return size == queue.length? true : false;
    }
}
