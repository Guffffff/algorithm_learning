package week01;

import java.util.ArrayDeque;
import java.util.Deque;

public class LargestRectangleArea {

    static class Rectangular{
        int high;
        int wide;
        public Rectangular(int high, int wide){
            this.high = high;
            this.wide = wide;
        }
    }


    public static int largestRectangleArea(int[] heights) {
        int answer = 0;
        Deque<Rectangular> stack = new ArrayDeque<>();
        for (int i = 0; i <= heights.length; i++){
            int value = 0;
            if (i == heights.length){
                value = 0;
            }else{
                value = heights[i];
            }
            int wide = 0;
            while(stack.size() > 0 && value < stack.peek().high){
                wide += stack.peek().wide;
                int valueTemp = stack.peek().high * wide;
                answer = answer > valueTemp? answer:valueTemp;
                stack.pop();
            }
            stack.push(new Rectangular(value, wide + 1));
        }
        return answer;
    }


    public static void main(String[] args){
        int[] heights = {2,1,5,6,2,3};
        int a = largestRectangleArea(heights);
        System.out.println(heights);
    }
}
