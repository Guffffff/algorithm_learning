package week01;

public class MergeTwoLists {

    /**
     * 合并两个有序链表
     * @param args
     */
    public static void main(String[] args){
        ListNode list1 = new ListNode(1,new ListNode(2));
        ListNode list2 = new ListNode(3,new ListNode(4, new ListNode(5)));
        mergeTwoLists(list1, list2);
    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null && list2 == null){
            return null;
        }else if (list1 == null){
            return list2;
        }else if (list2 == null){
            return list1;
        }
        ListNode node1 = list1;
        ListNode node2 = list2;
        ListNode result = new ListNode(-1);
        ListNode previousNode = result;
        while (true){
            if (node1 == null){
                previousNode.next = node2;
                break;
            } else if (node2 == null) {
                previousNode.next = node1;
                break;
            } else if (node1.val < node2.val){
                previousNode.next = new ListNode(node1.val);
                node1 = node1.next;
            } else{
                previousNode.next = new ListNode(node2.val);
                node2 = node2.next;
            }
            previousNode = previousNode.next;
        }
        return result.next;
    }


}
