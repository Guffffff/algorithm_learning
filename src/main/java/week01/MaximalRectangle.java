package week01;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 最大矩形
 */
public class MaximalRectangle {

    static class Rectangular{
        int high;
        int wide;
        public Rectangular(int high, int wide){
            this.high = high;
            this.wide = wide;
        }
    }


    public static int largestRectangleArea(int[] heights) {
        int answer = 0;
        Deque<LargestRectangleArea.Rectangular> stack = new ArrayDeque<>();
        for (int i = 0; i <= heights.length; i++){
            int value = 0;
            if (i == heights.length){
                value = 0;
            }else{
                value = heights[i];
            }
            int wide = 0;
            while(stack.size() > 0 && value < stack.peek().high){
                wide += stack.peek().wide;
                int valueTemp = stack.peek().high * wide;
                answer = answer > valueTemp? answer:valueTemp;
                stack.pop();
            }
            stack.push(new LargestRectangleArea.Rectangular(value, wide + 1));
        }
        return answer;
    }

    //1.矩阵构建为柱状图
    //2.循环计算柱状图
    public static int maximalRectangle(char[][] matrix) {
        int max = 0;
        int[] tempMatrix = new int[matrix[0].length];
        for(int rowNum = 0; rowNum < matrix.length; rowNum++){
            for (int i = 0; i < matrix[0].length; i++){
                int value = Integer.valueOf(matrix[rowNum][i]) - 48;
                if (rowNum > 0 && tempMatrix[i] >= 1 && value == 1){
                    tempMatrix[i] = tempMatrix[i] + 1;
                }else{
                    tempMatrix[i] = value;
                }
            }
            int tempValue = largestRectangleArea(tempMatrix);
            max = max > tempValue ? max : tempValue;
        }
        return max;
    }

    public static void main(String[] args){
        char[][] matrix = {{'1','0','1','0','0'},{'1','0','1','1','1'},{'1','1','1','1','1'},{'1','0','0','1','0'}};
        int max = maximalRectangle(matrix);
        System.out.println(max);
    }

}
