package week02;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 811. 子域名访问计数
 */
public class SubdomainVisits {

    public static List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();

        for (String str : cpdomains) {
            String[] arr = str.split(" ");
            int num = Integer.valueOf(arr[0]);

            String com = arr[1];
            String[] arr2 = com.split("\\.");
            for (int i = 0; i < arr2.length; i++) {
                StringBuilder str3 = new StringBuilder();
                for (int j = i; j < arr2.length; j++) {
                    if (j + 1 == arr2.length) {
                        str3.append(arr2[j]);
                    } else {
                        str3.append(arr2[j]).append(".");
                    }
                }

                if (map.containsKey(str3.toString())) {
                    map.put(str3.toString(), map.get(str3.toString()) + num);
                } else {
                    map.put(str3.toString(), num);
                }
            }

        }
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String str = entry.getValue() + " " + entry.getKey();
            result.add(str);
        }
        return result;
    }

}
