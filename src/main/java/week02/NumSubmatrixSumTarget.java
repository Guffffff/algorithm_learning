package week02;

import java.util.HashMap;
import java.util.Map;

/**
 * 1074. 元素和为目标值的子矩阵数量
 */
public class NumSubmatrixSumTarget {

    public int numSubmatrixSumTarget(int[][] matrix, int target) {
        int result = 0;
        for (int i = 0; i < matrix.length; i++) {
            int[] sum = new int[matrix[0].length];
            for (int j = i; j < matrix.length; j++) {
                for (int s = 0; s < matrix[0].length; s++){
                    sum[s] += matrix[j][s];
                }
                Map<Integer, Integer> map = new HashMap<>();
                map.put(0, 1);
                int perSum = 0;
                for (int n : sum){
                    perSum += n;
                    //判断
                    if (map.containsKey(perSum - target)){
                        result += map.get(perSum - target);
                    }
                    map.put(perSum, map.getOrDefault(perSum, 0) + 1);
                }
            }
        }
        return result;
    }

}
