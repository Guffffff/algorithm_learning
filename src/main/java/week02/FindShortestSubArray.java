package week02;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * 697. 数组的度
 */
public class FindShortestSubArray {

    /**
     * [1,2,2,3,1,4,2]
     * @param nums
     * @return
     */
    public static int test(int[] nums) {
        Map<Integer, LinkedList<Integer>> map = new HashMap<>();
        int max = 1;
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.get(nums[i]).add(i);
                max = max > map.get(nums[i]).size() ? max : map.get(nums[i]).size();
            } else {
                LinkedList<Integer> list = new LinkedList<>();
                list.add(i);
                map.put(nums[i], list);
            }
        }
        if (max == 1) {
            return 1;
        }
        int length = Integer.MAX_VALUE;
        for (Map.Entry<Integer, LinkedList<Integer>> entry : map.entrySet()) {
            if (entry.getValue().size() != max) {
                continue;
            }
            int value = entry.getValue().getLast() - entry.getValue().getFirst() + 1;
            length = length > value ? value : length;
        }
        return length;
    }

}
