package week02;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
    class Team {
        Integer key;
        Integer value;
        Team previousTeam;
        Team latterTeam;
    }

    Map<Integer, Team> map;

    Integer maxSize;

    Team headTeam;

    Team lastTeam;

    public LRUCache(int capacity) {
        this.map = new HashMap<>();
        this.maxSize = capacity;
        this.headTeam = new Team();
        this.lastTeam = headTeam;
    }

    public int get(int key) {
        if (!this.map.containsKey(key)) {
            return -1;
        }
        //更新优先级
        Team team = map.get(key);
        //链表移位
        Team previousTeam = team.previousTeam;
        Team latterTeam = team.latterTeam;
        if (latterTeam == null) {
            return team.value;
        } else {
            previousTeam.latterTeam = latterTeam;
            latterTeam.previousTeam = previousTeam;
            lastTeam.latterTeam = team;
            team.previousTeam = lastTeam;
            team.latterTeam = null;
            lastTeam = team;
        }
        return team.value;
    }

    public void put(int key, int value) {
        if (this.map.containsKey(key)) {
            Team team = map.get(key);
            team.value = value;
            //链表移位
            Team previousTeam = team.previousTeam;
            Team latterTeam = team.latterTeam;
            if (latterTeam == null) {
                return;
            } else {
                //插入最后
                previousTeam.latterTeam = latterTeam;
                latterTeam.previousTeam = previousTeam;
                lastTeam.latterTeam = team;
                team.previousTeam = lastTeam;
                team.latterTeam = null;
                lastTeam = team;
            }
        } else {
            Team team = new Team();
            team.key = key;
            team.value = value;
            //头判断
            if (this.map.size() == 0) {
                headTeam.latterTeam = team;
            }
            team.previousTeam = lastTeam;
            lastTeam.latterTeam = team;
            //更新优先级
            lastTeam = team;
            if (map.size() >= maxSize) {
                Team temp = headTeam.latterTeam;
                map.remove(temp.key);
                if (temp.latterTeam != null){
                    temp.latterTeam.previousTeam = headTeam;
                }
                headTeam.latterTeam = temp.latterTeam;
            }

            this.map.put(key, team);
        }
    }

    /**
     * ["LRUCache","get","get","put" ,"get", "put","put","put","put","get","put"]
     * [[1],[6],[8],[12,1] ,[2], [15,11],[5,2],[1,15],[4,2],[5],[15,15]]
     * @param args
     */

    public static void main(String[] args) {
        LRUCache lruCache = new LRUCache(1);
        System.out.println(lruCache.get(6));
        System.out.println(lruCache.get(8));
        lruCache.put(12, 1);
        System.out.println(lruCache.get(2));
        lruCache.put(15, 11);
        lruCache.put(5, 2);
        lruCache.put(1, 15);
        lruCache.put(4, 2);
        System.out.println(lruCache.get(5));
        lruCache.put(15, 15);



    }
}
